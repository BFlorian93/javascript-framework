System.config({
    transpiler: 'plugin-traceur',
    paths: {
        'jquery': './node_modules/jquery/dist/jquery.js',
        'traceur': './node_modules/traceur/bin/traceur.js',
        'traceur-runtime': './node_modules/traceur/bin/traceur-runtime.js',
        'plugin-traceur': './node_modules/systemjs-plugin-traceur/plugin-traceur.js'
    },
    meta: {
        'traceur': {
            format: 'global',
            exports: 'traceur'
        },
        'traceur-runtime': {
            format: 'global',
            exports: '$traceurRuntime'
        }
    },
    packages: {
        defaultExtension: true
    }
});