import Contact from '../models/contact.js';
import validator from '../utilities/validator.js';
import logger from '../utilities/logger.js';

export class ContactsController {
    /**
     * Class constructor.
     */
    constructor() {}

    /**
     * Get a listing of all contact.
     */
    index() {
        return Contact.getContacts();
    }

    /**
     * Create a new contact.
     * 
     * @param {Object} contact 
     */
    store(contact) {
        try {
            validator.validate({
                [contact.first_name]: 'required|string',
                [contact.last_name]: 'required|string',
                [contact.phone_number]: 'required|number|min:10',
                [contact.compnay]: 'string',
                [contact.email_address]: 'email',
                [contact.website]: 'string'
            });

            Contact.save(contact);
        } catch (e) {
            logger.log(`Date: ${new Date().toLocaleDateString()}
                        \n Time: ${new Date().toLocaleTimeString()}
                        \n Type: 'create-contact-error'
                        \n Errors: validator.errors`, 'error');

            throw e;
        }
    }

    /**
     * Update the given contact.
     * 
     * @param {Object} contact 
     * @param {String} id 
     */
    update(contact, id) {
        try {
            validator.validate({
                [contact.first_name]: 'required|string',
                [contact.last_name]: 'required|string',
                [contact.phone_number]: 'required|number|min:10',
                [contact.compnay]: 'string',
                [contact.email_address]: 'email',
                [contact.website]: 'string'
            });

            Contact.update(contact, id);
        } catch (e) {
            logger.log(`Date: ${new Date().toLocaleDateString()}
                        \n Time: ${new Date().toLocaleTimeString()}
                        \n Type: 'edit-contact-error'
                        \n Errors: validator.errors`, 'error');

            throw e;
        }
    }

    /**
     * Delete the given contact.
     * 
     * @param {Object} contact 
     */
    destroy(contact) {
        Contact.delete(contact);
    }
}