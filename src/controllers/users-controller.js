import User from '../models/user.js';
import validator from '../utilities/validator.js';
import logger from '../utilities/logger.js';

/**
 * A class that register and authenticate users.
 */
export class UsersController {
    /**
     * Class constructor.
     */
    constructor() {}

    /**
     * Create a new account.
     * 
     * @param {String} email 
     * @param {String} password 
     */
    register(email, password) {
        try {
            validator.validate({
                [email]: 'required|email',
                [password]: 'required|min:8'
            });

            User.save({ email, password });
        } catch (e) {
            logger.log(`Date: ${new Date().toLocaleDateString()}
                        \n Time: ${new Date().toLocaleTimeString()}
                        \n Type: 'register-error'
                        \n Errors: validator.errors`, 'error');
            throw e;
        }
    }

    /**
     * Authenticate the given user.
     * 
     * @param {String} email 
     * @param {String} password 
     */
    login(email, password) {
        try {
            validator.validate({
                [email]: 'required|email',
                [password]: 'required|min:8'
            });

            User.authenticate({ email, password });
        } catch (e) {
            logger.log(`Date: ${new Date().toLocaleDateString()}
                        \n Time: ${new Date().toLocaleTimeString()}
                        \n Type: 'auth-error'
                        \n Errors: validator.errors`, 'error');
            throw e;
        }
    }
}