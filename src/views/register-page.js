import { Page } from './page.js';
import { TextField } from '../utilities/ui/text-field.js';
import { Button } from '../utilities/ui/button.js';
import { UsersController } from '../controllers/users-controller.js';
import { ErrorHandler } from '../utilities/error-handler.js';
import logger from '../utilities/logger.js';

export class RegisterPage extends Page {
    /**
     * Class constructor.
     */
    constructor() {
        super('Register');
        this.userCtrl = new UsersController();
    }

    /**
     * Create Register page element.
     */
    createElement() {
        super.createElement();

        const text = '<h1>Sign Up</h1>';
        this.element.append(text);

        this.createRegisterForm();
    }

    /**
     * Create the form for registration.
     */
    createRegisterForm() {
        const emailInput = new TextField('text', 'Email');
        const passwordInput = new TextField('password', 'Password');
        const passwordConfirmationInput = new TextField('password', 'PasswordConfirmation');
        const submiButton = new Button('Sign Up', 'raised');
        submiButton.setStyleString('width: 25%;');

        emailInput.appendToElement(this.element);
        passwordInput.appendToElement(this.element);
        passwordConfirmationInput.appendToElement(this.element);
        submiButton.appendToElement(this.element);

        const signUpBtn = this.element.find('#sign_up_btn');
        signUpBtn.click(() => { this.signUp(); });
    }

    /**
     * Create a new user account.
     */
    signUp() {
        try {
            const email = this.element.find('#email').val();
            const password = this.element.find('#password').val();
            const passwordConfirmation = this.element.find('#passwordconfirmation').val();

            if (password !== passwordConfirmation) {
                return alert('Passwords don\'t match!');
            }

            this.userCtrl.register(email, password);

            logger.log('Account successfully created.', 'success');
            logger.log(`Date: ${new Date().toLocaleDateString()}
                        \n Time: ${new Date().toLocaleTimeString()}
                        \n Type: 'register-success'`, 'success');
            alert('Account successfully created.');
        } catch (e) {
            new ErrorHandler(e);
        }
    }

    /**
     * Get the string of register page element.
     */
    getElementString() {
        const styleString = `text-align: center; padding-top: 200px;`;

        return `<div style="${styleString}"></div>`;
    }
}