import { Page } from './page.js';
import { Image } from '../utilities/ui/image.js';

export class HomePage extends Page {
    /**
     * Class constructor.
     */
    constructor() {
        super('Home');
    }

    /**
     * Create Home page element.
     */
    createElement() {
        super.createElement();

        const text = '<h1>Welcome to My App</h1><br>';
        this.element.append(text);

        const image = new Image('../../images/javascript.png');
        image.appendToElement(this.element);
    }

    /**
     * Get the string of home page element.
     */
    getElementString() {
        const styleString = 'text-align: center; padding-top: 70px;';

        return `<div style="${styleString}"></div>`;
    }
}