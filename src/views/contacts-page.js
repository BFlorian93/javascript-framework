import { Page } from './page.js';
import { ContactsController } from '../controllers/contacts-controller.js';
import { Button } from '../utilities/ui/button.js';
import { TextField } from '../utilities/ui/text-field.js';
import { DataTable } from '../utilities/ui/data-table.js';
import { Dialog } from '../utilities/ui/dialog.js';
import { ErrorHandler } from '../utilities/error-handler.js';

export class ContactsPage extends Page {
    /**
     * Class constructor.
     */
    constructor() {
        super('Contacts');
        this.contactCtrl = new ContactsController();
        this.contacts = this.contactCtrl.index();
        this.dialog;
        this.selectedContact;
    }

    /**
     * Create contacts page element.
     */
    createElement() {
        super.createElement();
        this.createDataTable();
        this.onSelectedContact();
        this.createButtons();
        this.createAddContactModal();
        this.createEditContactModal();
    }

    /**
     * Create data table for contacts.
     */
    createDataTable() {
        const headers = 'First Name,Last Name,Phone Number,Company,Email Address,Website'.split(',');
        const dataTable = new DataTable(headers, this.contacts);
        dataTable.appendToElement(this.element);
    }

    /**
     * Create open modal, edit contact and delete contact buttons.
     */
    createButtons() {
        this.createOpenModalBtn();
        this.createEditContactBtn();
        this.createDeleteContactBtn();
    }

    /**
     * Create open modal button.
     */
    createOpenModalBtn() {
        // Create open modal button.
        const openModalBtn = new Button('Open Modal', 'show_modal');
        openModalBtn.setMaterialIcon('add');
        openModalBtn.setStyleString('position: absolute; right: 20px; bottom: 20px;');
        openModalBtn.appendToElement(this.element);

        // Add event handler for open modal button.
        const openModalButton = this.element.find('#open_modal_btn');
        openModalButton.click(() => {
            this.dialog = document.querySelector('#add_contact_dialog');
            this.dialog.showModal();
        });
    }

    /**
     * Create edit contact button.
     */
    createEditContactBtn() {
        const editContactBtn = new Button('Edit Contact', 'fab');
        editContactBtn.setMaterialIcon('edit');
        editContactBtn.setStyleString('position: absolute; right: 20px; top: 70px;');
        editContactBtn.appendToElement(this.element);

        // Add event handler for edit selected contact button.
        const editSelectedContactBtn = this.element.find('#edit_contact_btn');
        editSelectedContactBtn.click(() => {
            if (this.selectedContact && this.selectedContact.length === 1) {
                this.setSelectedContactVal();
                this.dialog = document.querySelector('#edit_contact_dialog');

                setTimeout(() => {
                    this.dialog.showModal();
                }, 0);
            } else if (this.selectedContact && this.selectedContact.length > 1) {
                alert('You must select only 1 contact.');
            } else {
                alert('Nothing selected to edit.');
            }
        });
    }

    /**
     * Set the selected contact.
     */
    onSelectedContact() {
        this.element.find('tr').click(() => {
            setTimeout(() => {
                this.selectedContact = this.element.find('.is-selected');
            }, 0);
        });
    }

    /**
     * Create delete contact button.
     */
    createDeleteContactBtn() {
        const deleteContactBtn = new Button('Delete Contact', 'fab');
        deleteContactBtn.setMaterialIcon('delete');
        deleteContactBtn.setStyleString('position: absolute; right: 20px; top: 150px;');
        deleteContactBtn.appendToElement(this.element);

        // Add event handler for delete selected contact button.
        const deleteSelectedContactBtn = this.element.find('#delete_contact_btn');
        deleteSelectedContactBtn.click(() => {
            if (this.selectedContact && this.selectedContact.length === 1) {
                const contact = this.getSelectedContactVal();
                this.contactCtrl.destroy(contact);
                this.selectedContact.remove();
            } else if (this.selectedContact && this.selectedContact.length > 1) {
                alert('You must select only 1 contact.');
            } else {
                alert('Nothing selected to delete.');
            }
        });
    }

    /**
     * Create the modal to add a new contact.
     */
    createAddContactModal() {
        const dialog = new Dialog('Add Contact');
        // Add content text to the dialog.
        dialog.addContentText('<h4>Create a new contact</h4><hr>');
        // Add button to dialog.
        const addContactBtn = new Button('Add Contact', 'raised');
        addContactBtn.setStyleString('text-align: center;');
        dialog.addButton(addContactBtn);

        dialog.appendToElement(this.element);

        const content = this.element.find('#add_contact_dialog>.mdl-dialog__content');

        // Add inputs to the content of dialog.
        const firstNameInput = new TextField('text', 'First name');
        firstNameInput.appendToElement(content);
        const lastNameInput = new TextField('text', 'Last name');
        lastNameInput.appendToElement(content);
        const phoneNumberInput = new TextField('numeric', 'Phone number');
        phoneNumberInput.appendToElement(content);
        const companyInput = new TextField('text', 'Company');
        companyInput.appendToElement(content);
        const emailAddressInput = new TextField('text', 'Email address');
        emailAddressInput.appendToElement(content);
        const websiteInput = new TextField('text', 'Website');
        websiteInput.appendToElement(content);

        // Add event handler for create contact button.
        const createContactBtn = this.element.find('#add_contact_btn');
        createContactBtn.click(() => { this.addNewContact(); });

        // Add event handler for close modal button.
        const closeModalButton = this.element.find('#close_modal_btn');
        closeModalButton.click(() => { this.dialog.close(); });
    }

    /**
     * Create a modal to edit an existing contact.
     */
    createEditContactModal() {
        const dialog = new Dialog('Edit Contact');
        // Add content text to the dialog.
        dialog.addContentText('<h4>Edit contact</h4><hr>');
        // Add button to dialog.
        const editContactBtn = new Button('Edit Contact', 'raised');
        editContactBtn.setStyleString('text-align: center;');
        dialog.addButton(editContactBtn);

        dialog.appendToElement(this.element);

        const content = this.element.find('#edit_contact_dialog>.mdl-dialog__content');

        // Add inputs to the content of dialog.
        const firstNameInput = new TextField('text', 'First name');
        firstNameInput.appendToElement(content);
        const lastNameInput = new TextField('text', 'Last name');
        lastNameInput.appendToElement(content);
        const phoneNumberInput = new TextField('numeric', 'Phone number');
        phoneNumberInput.appendToElement(content);
        const companyInput = new TextField('text', 'Company');
        companyInput.appendToElement(content);
        const emailAddressInput = new TextField('text', 'Email address');
        emailAddressInput.appendToElement(content);
        const websiteInput = new TextField('text', 'Website');
        websiteInput.appendToElement(content);

        // Add event handler for create contact button.
        const updateContactBtn = this.element.find('#edit_contact_dialog').find('#edit_contact_btn');
        updateContactBtn.click(() => { this.editContact(); });

        // Add event handler for close modal button.
        const closeModalButton = this.element.find('#close_modal_btn');
        closeModalButton.click(() => { this.dialog.close(); });
    }

    /**
     * Add a new contact.
     */
    addNewContact() {
        try {
            const firstName = this.element.find('#add_contact_dialog').find('#first_name').val();
            const lastName = this.element.find('#add_contact_dialog').find('#last_name').val();
            const phoneNumber = this.element.find('#add_contact_dialog').find('#phone_number').val();
            const company = this.element.find('#add_contact_dialog').find('#company').val() || '';
            const emailAddress = this.element.find('#add_contact_dialog').find('#email_address').val() || '';
            const website = this.element.find('#add_contact_dialog').find('#website').val() || '';

            const contact = {
                first_name: firstName,
                last_name: lastName,
                phone_number: phoneNumber,
                company,
                email_address: emailAddress,
                website
            };

            this.contactCtrl.store(contact);

            const tbody = this.element.find('tbody');
            const newContact = `
                <tr> 
                    <td><label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect mdl-data-table__select mdl-js-ripple-effect--ignore-events is-upgraded" data-upgraded=",MaterialCheckbox"><input type="checkbox" class="mdl-checkbox__input"><span class="mdl-checkbox__focus-helper"></span><span class="mdl-checkbox__box-outline"><span class="mdl-checkbox__tick-outline"></span></span><span class="mdl-checkbox__ripple-container mdl-js-ripple-effect mdl-ripple--center"><span class="mdl-ripple"></span></span></label></td>
                    <td class="mdl-data-table__cell--non-numeric">${contact.first_name}</td>
                    <td class="mdl-data-table__cell--non-numeric">${contact.last_name}</td>
                    <td class="mdl-data-table__cell--non-numeric">${contact.phone_number}</td>
                    <td class="mdl-data-table__cell--non-numeric">${contact.company}</td>
                    <td class="mdl-data-table__cell--non-numeric">${contact.email_address}</td>
                    <td class="mdl-data-table__cell--non-numeric">${contact.website}</td>
                </tr>`;

            tbody.append(newContact);
            this.clearInputs();
            this.dialog.close();
        } catch (e) {
            new ErrorHandler(e);
        }
    }

    /**
     * Update the details of the given contact.
     */
    editContact() {
        try {
            const firstName = this.element.find('#edit_contact_dialog').find('#first_name').val();
            const lastName = this.element.find('#edit_contact_dialog').find('#last_name').val();
            const phoneNumber = this.element.find('#edit_contact_dialog').find('#phone_number').val();
            const company = this.element.find('#edit_contact_dialog').find('#company').val();
            const emailAddress = this.element.find('#edit_contact_dialog').find('#email_address').val();
            const website = this.element.find('#edit_contact_dialog').find('#website').val();

            let updatedContact = {
                first_name: firstName,
                last_name: lastName,
                phone_number: phoneNumber,
                company,
                email_address: emailAddress,
                website
            };

            this.contactCtrl.update(updatedContact, this.selectedContact.attr('id'));

            const editedContact = `
                <td><label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect mdl-data-table__select mdl-js-ripple-effect--ignore-events is-upgraded" data-upgraded=",MaterialCheckbox,MaterialRipple"><input type="checkbox" class="mdl-checkbox__input"><span class="mdl-checkbox__focus-helper"></span><span class="mdl-checkbox__box-outline"><span class="mdl-checkbox__tick-outline"></span></span><span class="mdl-checkbox__ripple-container mdl-js-ripple-effect mdl-ripple--center"><span class="mdl-ripple"></span></span></label></td>
                <td class="mdl-data-table__cell--non-numeric">${updatedContact.first_name}</td>
                <td class="mdl-data-table__cell--non-numeric">${updatedContact.last_name}</td>
                <td class="mdl-data-table__cell--non-numeric">${updatedContact.phone_number}</td>
                <td class="mdl-data-table__cell--non-numeric">${updatedContact.company}</td>
                <td class="mdl-data-table__cell--non-numeric">${updatedContact.email_address}</td>
                <td class="mdl-data-table__cell--non-numeric">${updatedContact.website}</td>
            `;

            this.selectedContact.html(editedContact);
            this.clearInputs();
            this.dialog.close();
        } catch (e) {
            new ErrorHandler(e);
        }
    }

    /**
     * Clear modal inputs.
     */
    clearInputs() {
        this.element.find('#first_name').val('');
        this.element.find('#last_name').val('');
        this.element.find('#phone_number').val('');
        this.element.find('#company').val('');
        this.element.find('#email_address').val('');
        this.element.find('#website').val('');
    }

    /**
     * Set the values of selected contact.
     */
    setSelectedContactVal() {
        const selectedContact = this.getSelectedContactVal();

        this.element.find('#edit_contact_dialog').find('#first_name').val(selectedContact.first_name);
        this.element.find('#edit_contact_dialog').find('#last_name').val(selectedContact.last_name);
        this.element.find('#edit_contact_dialog').find('#phone_number').val(selectedContact.phone_number);
        this.element.find('#edit_contact_dialog').find('#company').val(selectedContact.company);
        this.element.find('#edit_contact_dialog').find('#email_address').val(selectedContact.email_address);
        this.element.find('#edit_contact_dialog').find('#website').val(selectedContact.website);
    }

    getSelectedContactVal() {
        const firstName = this.selectedContact.find('td').eq(1).html();
        const lastName = this.selectedContact.find('td').eq(2).html();
        const phoneNumber = this.selectedContact.find('td').eq(3).html();
        const company = this.selectedContact.find('td').eq(4).html();
        const emailAddress = this.selectedContact.find('td').eq(5).html();
        const website = this.selectedContact.find('td').eq(6).html();

        const selectedContact = {
            first_name: firstName,
            last_name: lastName,
            phone_number: phoneNumber,
            company,
            email_address: emailAddress,
            website
        }

        return selectedContact;
    }

    /**
     * Get the string of contacts page element.
     */
    getElementString() {
        const styleString = `
            padding-top: 70px;
            display: flex;
            justify-content: center;
            align-items: center;
            text-align: center;
        `;

        return `<div style="${styleString}"></div>`;
    }
}