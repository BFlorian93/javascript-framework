import { BaseElement } from '../utilities/ui/base-element.js';

export class Page extends BaseElement {
    /**
     * Class constructor.
     */
    constructor(pageTitle) {
        super();
        this.pageTitle = pageTitle;
    }
}