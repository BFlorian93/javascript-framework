import { Page } from './page.js';
import { TextField } from '../utilities/ui/text-field.js';
import { Button } from '../utilities/ui/button.js';
import { UsersController } from '../controllers/users-controller.js';
import { ErrorHandler } from '../utilities/error-handler.js';
import logger from '../utilities/logger.js';

export class LoginPage extends Page {
    /**
     * Class constructor.
     */
    constructor() {
        super('Login');
        this.userCtrl = new UsersController();
    }

    /**
     * Create Login page element.
     */
    createElement() {
        super.createElement();

        const text = '<h1>Log In</h1>';
        this.element.append(text);

        this.createLoginForm();
    }

    /**
     * Create the form for authentication.
     */
    createLoginForm() {
        const emailInput = new TextField('text', 'Email');
        const passwordInput = new TextField('password', 'Password');
        const submiButton = new Button('Log in', 'raised');
        submiButton.setStyleString('width: 25%;');

        emailInput.appendToElement(this.element);
        passwordInput.appendToElement(this.element);
        submiButton.appendToElement(this.element);

        const loginBtn = this.element.find('#log_in_btn');
        loginBtn.click(() => { this.auth(); });
    }

    /**
     * Authenticate the user.
     */
    auth() {
        try {
            const emailAddress = this.element.find('#email').val();
            const password = this.element.find('#password').val();

            this.userCtrl.login(emailAddress, password);
            localStorage.setItem('authenticated', btoa('true'));

            logger.log(`Date: ${new Date().toLocaleDateString()}
                \n Time: ${new Date().toLocaleTimeString()}
                \n Type: 'auth-success'`, 'success');

            alert(`Welcome back ${emailAddress}`);
            location.reload();
        } catch (e) {
            new ErrorHandler(e);
        }
    }

    /**
     * Get the string of login page element.
     */
    getElementString() {
        const styleString = `text-align: center; padding-top: 200px;`;

        return `<div style="${styleString}"></div>`;
    }
}