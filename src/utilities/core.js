import { NavigationBar } from './ui/navigation-bar.js';

export class Core {
    /**
     * Class constructor.
     * 
     * @param {String} title 
     */
    constructor(title) {
        this.title = title;
        this.navBar = new NavigationBar(this.title);
        this.routeMap = {};
        this.defaultRoute = null;
    }

    /**
     * Add a new route to the navigation bar.
     * 
     * @param {String} id 
     * @param {String} link 
     * @param {Object} pageObject 
     * @param {Boolean} defaultRoute 
     */
    addRoute(id, link = null, pageObject, defaultRoute = false) {
        this.navBar.addLink(id, link);
        this.routeMap[id] = pageObject;

        if (defaultRoute) {
            this.defaultRoute = id;
        }
    }

    /**
     * Activate the given route.
     * 
     * @param {String} route 
     */
    activateRoute(route) {
        const content = this.navBar.element.find('.page-content');
        content.empty();

        this.routeMap[route].appendToElement(content);
    }

    /**
     * 
     * 
     * @param {Object} element 
     */
    show(element) {
        this.navBar.appendToElement(element);

        this.navBar.element.find('.mdl-navigation__link').click((event) => {
            const route = event.target.innerHTML;
            this.activateRoute(route.trim());
        });

        if (this.defaultRoute) {
            this.activateRoute(this.defaultRoute);
        }
    }
}