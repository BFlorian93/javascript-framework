import logger from './logger.js';

/**
 * A class that handle errors.
 */
export class ErrorHandler {
    /**
     * Class constructor.
     */
    constructor(error) {
        this.error = error;
        this.handle(this.error);
    }

    /**
     * Handle the given error.
     * 
     * @param {any} e 
     */
    handle(e) {
        if (typeof e === 'string' || e instanceof Error) {
            alert(e);
        } else if (typeof e === 'object') {
            let errors = '';

            for (const error of e) {
                const key = Object.keys(error);

                errors += `${key} ${error[key]} \n`;
            }

            logger.log(errors, 'warning');
            alert('Check your console to see the errors.');
        }
    }
}