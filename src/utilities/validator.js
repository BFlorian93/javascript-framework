import logger from './logger.js';

/**
 * A class that validates data.
 */
class Validator {
    /**
     * Class constructor.
     */
    constructor() {
        this.errors = [];
    }

    /**
     * Validate the given data.
     * 
     * @param {Object} data 
     */
    validate(data) {
        for (const key in data) {
            let rules = data[key].split('|');

            for (const rule of rules) {
                let min;

                if (rule.startsWith('min')) {
                    min = this.getMinFromRule(rule);
                }

                switch (rule) {
                    case 'required':
                        if (!this.isNotEmpty(key)) {
                            this.errors.push({
                                [key]: `is required.`
                            });
                        }
                        break;

                    case 'email':
                        if (!this.isEmail(key)) {
                            this.errors.push({
                                [key]: `is not a valid email address.`
                            });
                        }
                        break;

                    case 'string':
                        if (!this.isString(key)) {
                            this.errors.push({
                                [key]: `is not a string.`
                            });
                        }
                        break;

                    case 'number':
                        if (!this.isNumber(key)) {
                            this.errors.push({
                                [key]: `is not a number.`
                            });
                        }
                        break;

                    case `min:${min}`:
                        if (!this.isMin(key, min)) {
                            this.errors.push({
                                [key]: `length must be equal or greater than ${min}.`
                            });
                        }
                        break;

                    default:
                        logger.log('Invalid rule.', 'error');
                        break;
                }

                rules = null;
            }
        }

        if (this.errors.length > 0) {
            const errors = this.errors;
            this.errors = [];
            throw errors;
        }
    }

    /**
     * Verify if thr given value it's a valid email address.
     * 
     * @param {String} value 
     */
    isEmail(value) {
        // Email Address Regular Expression
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(value);
    }

    /**
     * Verify if the given value it's not empty.
     * 
     * @param {String} value 
     */
    isNotEmpty(value) {
        if (value) {
            return !!true;
        }
    }

    /**
     * Verify if the given value it's lower than the minimum.
     * 
     * @param {String} value 
     * @param {String} min 
     */
    isMin(value, min) {
        if (value.length >= min) {
            return !!true;
        }
    }

    /**
     * Verifi if the given value it's a number.
     * 
     * @param {String} value 
     */
    isNumber(value) {
        const pattern = /^\d+$/;
        return pattern.test(value);
    }

    /**
     * Verifi if the given value it's a string.
     * 
     * @param {String} value 
     */
    isString(value) {
        if (!this.isNumber(value) && typeof value === 'string') {
            return !!true;
        }
    }

    /**
     * Get the minimum value from a rule.
     * 
     * @param {String} value 
     */
    getMinFromRule(value) {
        let min;

        if (value.length - 1 > 4) {
            min = value.substring(4, value.length);
        } else {
            min = value.charAt(4);
        }

        return min;
    }
}

/**
 * Export a new instance of Validator class.
 */
export default new Validator();