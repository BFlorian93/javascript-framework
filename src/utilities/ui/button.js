import { BaseElement } from './base-element.js';

export class Button extends BaseElement {
    /**
     * Class constructor.
     * 
     * @param {String} name 
     * @param {String} type 
     */
    constructor(name, type) {
        super();
        this.name = name;
        this.type = type;
        this.icon = '';
        this.styleString = '';
    }

    /**
     * Set the styles of the button.
     * 
     * @param {String} style 
     */
    setStyleString(style) {
        this.styleString = style;
    }

    /**
     * Get the id of the button.
     */
    getButtonId() {
        let id;

        if (this.name.includes(' ')) {
            id = this.name.replace(' ', '_').toLowerCase() + '_btn';
        } else {
            id = this.name.toLowerCase() + '_btn';
        }

        return id;
    }

    setMaterialIcon(icon) {
        this.icon = icon;
    }

    /**
     * Get the string of the button element.
     */
    getElementString() {
        switch (this.type) {
            case 'raised':
                return `
                    <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored"
                        style="${this.styleString}" id=${this.getButtonId()}>
                        ${this.name}
                    </button>
                `;

            case 'fab':
                return `
                    <button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored" style="${this.styleString}" id=${this.getButtonId()}>
                        <i class="material-icons">${this.icon}</i>
                    </button>`;

            case 'show_modal':
                return `
                    <button type="button" class="mdl-button show-modal mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored" style="${this.styleString}" id=${this.getButtonId()}>
                        <i class="material-icons">${this.icon}</i>
                    </button>
                `;

            default:
                console.log('Invalid button type.');
                break;
        }

    }
}