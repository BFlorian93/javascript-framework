import $ from 'jquery';

export class BaseElement {
    /**
     * Class constructor.
     */
    constructor() {
        this.element = null; // jQuery Object.
    }

    /**
     * Insert content at the end of the selected element.
     * 
     * @param {String} el 
     */
    appendToElement(el) {
        this.createElement();
        el.append(this.element);
        this.enableJS();
    }

    /**
     *  Create jQuery element selector.
     */
    createElement() {
        const s = this.getElementString();
        this.element = $(s);
    }

    /**
     * Get the string of the element.
     */
    getElementString() {
        throw 'Please override getElementString() in BaseElement!';
    }

    /**
     * Enable JavaScript for the given element.
     */
    enableJS() {
        componentHandler.upgradeElement(this.element[0]);
    }
}