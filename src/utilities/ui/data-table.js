import { BaseElement } from './base-element.js';

export class DataTable extends BaseElement {
    /**
     * Class constructor.
     */
    constructor(headers, data) {
        super();
        this.headers = headers;
        this.data = data;
    }

    /**
     * Get the string of the data table element.
     */
    getElementString() {
        let thTags = '';

        for (const header of this.headers) {
            thTags += `<th class="mdl-data-table__cell--non-numeric">${header}</th>`;
        }

        let trTags = '';
        for (const row of this.data) {
            trTags += `<tr id="${this.data.indexOf(row)}">`;

            for (const property of this.headers) {
                let field;

                if (property.trim().includes(' ')) {
                    field = row[property.replace(' ', '_').toLowerCase()];
                } else {
                    field = row[property.toLowerCase()];
                }

                trTags += `<td class="mdl-data-table__cell--non-numeric">${field}</td>`;
            }

            trTags += `</tr>`;
        }

        return `
            <table class="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp">
                <thead>
                    <tr>
                    ${thTags}
                    </tr>
                </thead>
                <tbody>
                   ${trTags}
                </tbody>
            </table>
        `;
    }
}