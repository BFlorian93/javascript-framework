import { BaseElement } from './base-element.js';

export class Image extends BaseElement {
    /**
     * Class constructor.
     */
    constructor(fileName) {
        super();
        this.fileName = fileName;
        this.styleString = '';
    }

    /**
     * Set the styles of the image.
     * 
     * @param {String} style 
     */
    setStyleString(style) {
        this.styleString = style;
    }

    /**
     * Get the string of the image element.
     */
    getElementString() {
        return `
            <img src="${this.fileName}" style="${this.styleString}"></img>
        `;
    }
}