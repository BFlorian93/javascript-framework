import { BaseElement } from './base-element.js';

export class TextField extends BaseElement {
    /**
     * Class constructor.
     * 
     * @param {String} type 
     * @param {String} placeholder 
     * @param {String} rows 
     */
    constructor(type, placeholder, rows = 3) {
        super();
        this.type = type;
        this.placeholder = placeholder;
        this.rows = rows;
    }

    /**
     * Get the string of text filed element.
     */
    getElementString() {
        let id = '';

        if (this.placeholder.includes(' ')) {
            id = this.placeholder.replace(' ', '_').toLowerCase();
        } else {
            id = this.placeholder.toLowerCase();
        }

        switch (this.type) {
            case 'text':
                return `
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input class="mdl-textfield__input" type="text" id="${id}">
                        <label class="mdl-textfield__label" for="${this.placeholder.toLowerCase()}">${this.placeholder}</label>
                    </div><br>`;

            case 'password':
                return `
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input class="mdl-textfield__input" type="password" id="${id}">
                        <label class="mdl-textfield__label" for="${this.placeholder.toLowerCase()}">${this.placeholder}</label>
                    </div><br>`;

            case 'multiline-text':
                return `
                <div class="mdl-textfield mdl-js-textfield">
                    <textarea class="mdl-textfield__input" type="text" rows= "${this.rows}" id="${id}" ></textarea>
                    <label class="mdl-textfield__label" for="${this.placeholder.toLowerCase()}">${this.placeholder}</label>
                </div><br>`;

            case 'numeric':
                return `
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="${id}">
                        <label class="mdl-textfield__label" for="${this.placeholder.toLowerCase()}">${this.placeholder}</label>
                        <span class="mdl-textfield__error">Input is not a number!</span>
                    </div><br>`;
            default:
                break;
        }
    }
}