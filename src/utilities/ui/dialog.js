import { BaseElement } from './base-element.js';

export class Dialog extends BaseElement {
    /**
     * Class constructor.
     */
    constructor(name) {
        super();
        this.name = name;
        this.contentText = '';
        this.buttons = [];
    }

    /**
     * Get the id of the dialog.
     */
    getDialogId() {
        let id;

        if (this.name.includes(' ')) {
            id = this.name.replace(' ', '_').toLowerCase() + '_dialog';
        } else {
            id = this.name.toLowerCase() + '_dialog';
        }

        return id;
    }

    /**
     * Get the string of dialog element.
     */
    getElementString() {
        let buttons = '';

        for (const button of this.buttons) {
            buttons += button + '<br>';
        }

        return `
            <dialog class="mdl-dialog" id =${this.getDialogId()}>
                <div class="mdl-dialog__content">
                    ${this.contentText}
                </div>
                <div class="mdl-dialog__actions mdl-dialog__actions--full-width">
                    ${buttons}
                <button type="button" class="mdl-button mdl-js-button mdl-button--accent close" style="text-align: center;" id="close_modal_btn">Close</button>
                </div>
            </dialog>
        `;
    }

    /**
     * Add text to the content of dialog.
     * 
     * @param {String} text 
     */
    addContentText(text) {
        this.contentText = text;
    }

    /**
     * Add button to the dialog.
     * 
     * @param {String} button 
     */
    addButton(button) {
        this.buttons.push(button.getElementString().trim());
    }
}