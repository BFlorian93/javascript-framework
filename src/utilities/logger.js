/**
 * A class that logs messages to the console.
 */
class Logger {
    /**
     * Class constructor.
     */
    constructor() {
        this.textSuccess = 'background: green; color: white; display: block; font-size: 14px;';
        this.textWarning = 'background: yellow; color: black; display: block; font-size: 14px;';
        this.textError = 'background: red; color: white; display: block; font-size: 14px;';
    }

    /**
     * Log the given message to the console.
     * 
     * @param {String} message 
     * @param {String} type 
     */
    log(message, type) {
        switch (type) {
            case 'success':
                console.log(`%c ${'***'.repeat(7)} ${type.toUpperCase()} ${'***'.repeat(7)}`, this.textSuccess);
                console.log(`%c ${'~~~'.repeat(17)}`, this.textSuccess);
                console.log(`%c ${message}`, this.textSuccess);
                console.log(`%c ${'~~~'.repeat(17)}`, this.textSuccess);
                break;
            case 'warning':
                console.log(`%c ${'---'.repeat(7)}> ${type.toUpperCase()} <${'---'.repeat(7)}`, this.textWarning);
                console.log(`%c ${'~~~'.repeat(17)}`, this.textWarning);
                console.log(`%c ${message}`, this.textWarning);
                console.log(`%c ${'~~~'.repeat(17)}`, this.textWarning);
                break;
            case 'error':
                console.log(`%c ${'==='.repeat(7)}> ${type.toUpperCase()} <${'==='.repeat(7)}`, this.textError);
                console.log(`%c ${'~~~'.repeat(17)}`, this.textError);
                console.log(`%c ${message}`, this.textError);
                console.log(`%c ${'~~~'.repeat(17)}`, this.textError);
                break;

            default:
                console.log(`%c ${'=>'.repeat(7)} Invalid type. ${'<='.repeat(7)}`, this.textError);
                break;
        }
    }
}

/**
 * Export a new instance of Logger class.
 */
export default new Logger();