import { Core } from './utilities/core.js';
import { LoginPage } from './views/login-page.js';
import { RegisterPage } from './views/register-page.js';
import { HomePage } from './views/home-page.js';
import { ContactsPage } from './views/contacts-page.js';

class App extends Core {
    /**
     * Class constructor.
     */
    constructor() {
        super('My App');

        this.addRoutes();
    }

    /**
     * Add routes to the application.
     */
    addRoutes() {
        if (localStorage.getItem('authenticated') === btoa('true')) {
            this.addRoute('Home', '#home', new HomePage(), true);
            this.addRoute('Contacts', '#contacts', new ContactsPage());
        } else {
            this.addRoute('Login', '#login', new LoginPage(), true);
            this.addRoute('Register', '#register', new RegisterPage());
        }
    }
}

/**
 * Export a new instance of App class.
 */
export let application = new App();