/**
 * A class that represents a user model.
 */
class User {

    /**
     * Class constructor.
     */
    constructor() {
        if (!localStorage.getItem('accounts')) {
            localStorage.setItem('accounts', JSON.stringify([]));
        }
    }

    /**
     * Get all users accounts.
     */
    getAccounts() {
        return JSON.parse(localStorage.getItem('accounts'));
    }

    /**
     * Create a new accout.
     * 
     * @param {Object} user 
     */
    save(user) {
        if (!this.findUserByEmail(user.email)) {
            const accounts = this.getAccounts();

            const newUser = {
                email: user.email,
                password: btoa(user.password)
            };

            accounts.push(newUser);
            localStorage.setItem('accounts', JSON.stringify(accounts));
        } else {
            throw 'This email address is already taken.'
        }
    }

    /**
     * Authenticate the given user.
     * 
     * @param {Object} user 
     */
    authenticate(user) {
        const accounts = this.getAccounts();
        let userAccount = accounts.find(account => account.email === user.email && account.password === btoa(user.password));

        if (userAccount) {
            return userAccount;
        } else {
            throw new Error('User not found!');
        }
    }

    /**
     * Get a user by email.
     * 
     * @param {String} email 
     */
    findUserByEmail(email) {
        const accounts = this.getAccounts();
        let user = accounts.find(account => account.email === email);

        if (user) {
            return !!true;
        }
    }
}

/**
 * Export a new instance of User class.
 */
export default new User();