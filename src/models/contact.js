/**
 * A class that represents a contact model.
 */
class Contact {
    /**
     * Class constructor.
     */
    constructor() {
        if (!localStorage.getItem('contacts')) {
            localStorage.setItem('contacts', JSON.stringify([]));
        }
    }

    /**
     * Save the given contact to storage.
     * 
     * @param {Object} contact 
     */
    save(contact) {
        const contacts = this.getContacts();
        contacts.push(contact);

        localStorage.setItem('contacts', JSON.stringify(contacts));
    }

    /**
     * Get all contact.
     */
    getContacts() {
        return JSON.parse(localStorage.getItem('contacts'));
    }

    /**
     * Find the specified contact.
     * 
     * @param {Object} contact 
     */
    find(contact) {
        const contacts = this.getContacts();
        const foundContact = contacts.filter(c => c.first_name === contact.first_name && c.last_name === contact.last_name && c.phone_number === contact.phone_number);

        if (foundContact) {
            return foundContact;
        } else {
            throw 'No contact found!';
        }
    }

    /**
     * Find a contact by it's name.
     * 
     * @param {String} firstName
     * @param {String} lastName 
     */
    getContactByName(firstName, lastName) {
        const contacts = this.getContacts();
        const contact = contacts.filter(c => c.first_name === firstName && c.last_name === lastName);

        if (contact) {
            return contact;
        } else {
            throw Error('No contact found!');
        }
    }

    /**
     * Find a contact by it's phone number.
     * 
     * @param {String} phoneNumber 
     */
    getContactByPhoneNumber(phoneNumber) {
        const contacts = this.getContacts();
        const contact = contacts.filter(c => c.phone_number === phoneNumber);

        if (contact) {
            return contact;
        } else {
            throw Error('No contact found!');
        }
    }

    /**
     * Update the given contact.
     * 
     * @param {Object} contact 
     * @param {String} id 
     */
    update(contact, id) {
        const contacts = this.getContacts();
        contacts[id] = contact;

        localStorage.setItem('contacts', JSON.stringify(contacts));
    }

    /**
     * Remove the given contact from storage.
     * 
     * @param {Object} contact 
     */
    delete(contact) {
        const contacts = this.getContacts();
        const newContacts = contacts.filter(c => c.first_name !== contact.first_name && c.last_name !== contact.last_name && c.phone_number !== contact.phone_number);

        localStorage.setItem('contacts', JSON.stringify(newContacts));
    }
}

/**
 * Export a new instance of Contact class.
 */
export default new Contact();