# Simple JavaScript Framework

## Requirements

* [Node and npm](https://nodejs.org/)

## Installation

* Download the [repository](https://bitbucket.org/BFlorian93/javascript-framework/get/5239aca44cc8.zip)
* Install node modules: `npm install`
* Start the server: `npm run dev`